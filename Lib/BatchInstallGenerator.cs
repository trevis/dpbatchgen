﻿using dpbatchgen.Models;
using Microsoft.Extensions.DependencyInjection;
using Mustache;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static dpbatchgen.Program;

namespace dpbatchgen.Lib {
    public class BatchInstallGenerator {
        private readonly Logger _logger;
        private readonly IServiceProvider _services;
        private readonly ProgramOptions _options;

        public PluginInfo PluginInfo { get; set; }

        public string BatchInstallerOutPath { get; private set; }

        public string TempFile { get; private set; }

        public string FileExtension { get; private set; }

        public BatchInstallGenerator(Logger logger, IServiceProvider services, ProgramOptions options) {
            _logger = logger;
            _services = services;
            _options = options;
        }

        public async Task<bool> Generate() {
            FileExtension = Path.GetExtension(PluginInfo.DownloadUrl);
            TempFile = Path.Combine(Path.GetTempPath(), Path.GetFileName(PluginInfo.DownloadUrl));
            BatchInstallerOutPath = InterpolatePluginVariables(_options.OutPath);
            
            _logger.Log($"Generating installer for : {PluginInfo.Name} {PluginInfo.Version}");

            if (!await Download()) {
                return false;
            }

            var inspector = _services.GetService<PluginFileInspector>();
            inspector.PluginInfo = PluginInfo;
            inspector.PluginTempFilePath = TempFile;

            if (!inspector.IsValid()) {
                _logger.Error($"\t{PluginInfo.Name} is invalid");
                return false;
            }

            foreach (var pluginAssembly in inspector.PluginAssemblies) {
                _logger.Verbose($"\tFound {pluginAssembly.ComponentType}: {pluginAssembly.CoreClass} in {Path.GetFileName(pluginAssembly.FileName)}");
            }

            return GenerateBatchFile(inspector.PluginAssemblies, inspector.PluginFileType);
        }

        private async Task<bool> Download() {
            try {
                await Task.Run(() => {
                    using (var client = new WebClient()) {
                        _logger.Verbose($"\tDownloading: {PluginInfo.DownloadUrl} to {TempFile}");
                        client.DownloadFile(PluginInfo.DownloadUrl, TempFile);
                    }
                });
            }
            catch (Exception ex) {
                _logger.Error($"\tUnable to download: {PluginInfo.DownloadUrl}");
                _logger.Error(ex.ToString());
                return false;
            }

            return true;
        }

        private string InterpolatePluginVariables(string outPath) {
            return outPath
                .Replace("{name}", PluginInfo.Name)
                .Replace("{clsid}", PluginInfo.ClassId)
                .Replace("{id}", PluginInfo.Id)
                .Replace("{version}", PluginInfo.Version);
        }

        private bool GenerateBatchFile(List<PluginAssembly> pluginAssemblies, PluginFileInspector.FileType pluginFileType) {
            // TODO: better templating system?
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = @"dpbatchgen.Resources.install-template.bat.txt";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream)) {
                string template = reader.ReadToEnd();
                FormatCompiler compiler = new FormatCompiler() {
                    RemoveNewLines = false
                };
                Generator generator = compiler.Compile(template);
                string batchContents = generator.Render(new {
                    PluginInfo = PluginInfo,
                    PluginAssemblies = pluginAssemblies,
                    IsZip = pluginFileType == PluginFileInspector.FileType.ZIP,
                    IsDll = pluginFileType == PluginFileInspector.FileType.DLL,
                    FileName = Path.GetFileName(PluginInfo.DownloadUrl)
                });

                if (_options.DryRun) {
                    _logger.Log($"\tBatch Installer Path (DryRun): {BatchInstallerOutPath}");
                }
                else {
                    if (!Directory.Exists(Path.GetDirectoryName(BatchInstallerOutPath)))
                        Directory.CreateDirectory(Path.GetDirectoryName(BatchInstallerOutPath));
                    File.WriteAllText(BatchInstallerOutPath, batchContents);

                    _logger.Log($"\tWrote Batch Installer: {BatchInstallerOutPath}");
                }
            }

            return true;
        }
    }
}
