﻿using dpbatchgen.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser;
using static dpbatchgen.Program;

namespace dpbatchgen.Lib {
    public class CSVLoader {
        private ProgramOptions _options;
        private Logger _logger;

        public CSVLoader(ProgramOptions options, Logger logger) {
            _options = options;
            _logger = logger;
        }

        public bool TryLoadCSV(out List<PluginInfo> pluginInfos) {
            _logger.Verbose($"Loading decal_plugins.csv from: {_options.CSVPath}");

            if (!File.Exists(_options.CSVPath)) {
                pluginInfos = null;
                _logger.Error($"Could not find csv: {_options.CSVPath}");
                return false;
            }

            CsvParserOptions csvParserOptions = new CsvParserOptions(true, ',');
            CsvPluginInfoMapping csvMapper = new CsvPluginInfoMapping();
            CsvParser<PluginInfo> csvParser = new CsvParser<PluginInfo>(csvParserOptions, csvMapper);

            pluginInfos = csvParser
                .ReadFromFile(_options.CSVPath, Encoding.UTF8)
                .Select(m => m.Result)
                .ToList();

            _logger.Verbose($"Loaded {pluginInfos.Count} plugins from csv");

            return true;
        }
    }
}
