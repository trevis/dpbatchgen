﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static dpbatchgen.Program; 

namespace dpbatchgen.Lib {
    public class Logger {
        private ProgramOptions _options;

        public Logger(ProgramOptions options) {
            _options = options;
        }

        public void Verbose(string message) {
            if (_options.Verbose) {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(message);
                Console.ResetColor();
            }
        }

        public void Log(string message) {
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public void Error(string message) {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
