﻿using dpbatchgen.Models;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static dpbatchgen.Models.PluginAssembly;

namespace dpbatchgen.Lib {
    public class PluginFileInspector {
        private Logger _logger;
        public enum FileType {
            Unknown,
            DLL,
            ZIP
        }

        public PluginInfo PluginInfo { get; set; }
        public string PluginTempFilePath { get; set; }
        public string PluginTempBaseDirectory { get; set; }
        public FileType PluginFileType { get; private set; }

        public string DllFilePath { get; private set; }
        public string ZipTempExtractionPath { get; private set; }
        public List<PluginAssembly> PluginAssemblies { get; } = new List<PluginAssembly>();

        public PluginFileInspector(Logger logger) {
            _logger = logger;
        }

        public bool IsValid() {
            switch (Path.GetExtension(PluginTempFilePath).ToLower()) {
                case ".dll":
                    PluginFileType = FileType.DLL;
                    PluginTempBaseDirectory = Path.GetDirectoryName(PluginTempFilePath);
                    CheckForDecalComponent(PluginTempFilePath);
                    break;
                case ".zip":
                    PluginFileType = FileType.ZIP;
                    UnzipContents();
                    string[] files = Directory.GetFiles(ZipTempExtractionPath, "*.dll", SearchOption.AllDirectories);
                    PluginTempBaseDirectory = ZipTempExtractionPath;
                    foreach (var dll in files) {
                        CheckForDecalComponent(dll);
                    }
                    break;
                default:
                    PluginFileType = FileType.Unknown;
                    _logger.Error($"\tInvalid plugin file type for {PluginInfo.Name}: {Path.GetExtension(PluginInfo.DownloadUrl)}. Expected zip or dll. ({PluginInfo.DownloadUrl})");
                    return false;
            }

            return PluginAssemblies.Count > 0;
        }

        private bool CheckForDecalComponent(string pluginTempFilePath) {
            if (!File.Exists(pluginTempFilePath))
                return false;

            var assembly = AssemblyDefinition.ReadAssembly(pluginTempFilePath);
            var types = assembly.MainModule.Types.Skip(1);

            foreach (var type in types) {
                if (type.BaseType == null)
                    continue;

                if (type.BaseType.FullName.Equals("Decal.Adapter.PluginBase")) {
                    PluginAssemblies.Add(new PluginAssembly(PluginInfo, pluginTempFilePath.Replace(PluginTempBaseDirectory,""), type.FullName, DecalComponentType.Plugin));
                }
                else if (type.BaseType.FullName.Equals("Decal.Adapter.FilterBase")) {
                    PluginAssemblies.Add(new PluginAssembly(PluginInfo, pluginTempFilePath.Replace(PluginTempBaseDirectory, ""), type.FullName, DecalComponentType.NetworkFilter));
                }
                else if (type.BaseType.FullName.Equals("Decal.Adapter.ServiceBase")) {
                    // TODO
                }
            }

            return PluginAssemblies.Count > 0;
        }

        private void UnzipContents() {
            ZipTempExtractionPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            _logger.Verbose($"\tUnzipping: {PluginTempFilePath} to {ZipTempExtractionPath}");
            Directory.CreateDirectory(ZipTempExtractionPath);
            ZipFile.ExtractToDirectory(PluginTempFilePath, ZipTempExtractionPath, true);
        }
    }
}
