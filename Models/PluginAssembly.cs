﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dpbatchgen.Models {
    public class PluginAssembly {
        public enum DecalComponentType {
            Plugin,
            NetworkFilter
        }

        public PluginInfo PluginInfo { get; set; }

        public string FileName { get; set; }
        public string AssemblyName { get => Path.GetFileName(FileName); }
        public string AssemblyPath { get => Path.Combine(PluginInfo.DefaultInstallDirectory, FileName.Replace(AssemblyName, "").TrimStart('\\')); }
        public string CoreClass { get; set; }
        public DecalComponentType ComponentType { get; set; }

        public PluginAssembly(PluginInfo info, string fileName, string coreClass, DecalComponentType componentType) {
            PluginInfo = info;
            FileName = fileName;
            CoreClass = coreClass;
            ComponentType = componentType;
        }
    }
}
