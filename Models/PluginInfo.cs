﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser.Mapping;

namespace dpbatchgen.Models {
    public class PluginInfo {
        /// <summary>
        /// ACCPP ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Plugin ClassId / GUID
        /// </summary>
        public string ClassId { get; set; }

        /// <summary>
        /// Plugin Website
        /// </summary>
        public string Codebase { get; set; }

        /// <summary>
        /// Plugin version as string
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Plugin name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Direct link to plugin / installer download
        /// </summary>
        public string DownloadUrl { get; set; }

        /// <summary>
        /// Default plugin install directory (preferred)
        /// </summary>
        public string DefaultInstallDirectory { get; set; }

        /// <summary>
        /// Wether or not to generate a batch installer for this plugin
        /// </summary>
        public bool GenerateBatchInstaller { get; set; }
    }

    internal class CsvPluginInfoMapping : CsvMapping<PluginInfo> {
        public CsvPluginInfoMapping()
            : base() {
            // ID,plugin clsid,codebase,version,name,Direct URL,installDir,generateBatch,Hash,NewImage
            MapProperty(0, x => x.Id);
            MapProperty(1, x => x.ClassId);
            MapProperty(2, x => x.Codebase);
            MapProperty(3, x => x.Version);
            MapProperty(4, x => x.Name);
            MapProperty(5, x => x.DownloadUrl);
            MapProperty(6, x => x.DefaultInstallDirectory);
            MapProperty(7, x => x.GenerateBatchInstaller);
        }
    }
}
