﻿using CommandLine;
using dpbatchgen.Lib;
using dpbatchgen.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser;

namespace dpbatchgen {
    /// <summary>
    /// Reads decal_plugins.csv and generates batch installers for any plugins needing it.
    /// </summary>
    public class Program {
        public class ProgramOptions {
            [Option('o', "out-path", Required = false, HelpText = "The output path for each plugin batch installer.  You can use variables {name}, {version}, {clsid}, and {id}", Default = "./decal plugins/current/{name}/{version}/{name}-{version}-install.bat")]
            public string OutPath { get; set; }

            [Option('c', "csv-path", Required = false, HelpText = "Path to decal_plugins.csv, containing all the plugin info", Default = "./decal_plugins.csv")]
            public string CSVPath { get; set; }

            [Option('d', "dry-run", Required = false, HelpText = "Dry Run. Don't actually write any batch installer files.", Default = false)]
            public bool DryRun { get; set; }

            [Option('v', "verbose", Required = false, HelpText = "Verbose output", Default = false)]
            public bool Verbose { get; set; }
        }

        static void Main(string[] args) {
             Parser.Default.ParseArguments<ProgramOptions>(args)
                 .WithParsed<ProgramOptions>(options => {
                     var logger = new Logger(options);

                     using (var services = ConfigureServices(options, logger)) {
                         var csvLoader = services.GetRequiredService<CSVLoader>();

                         List<PluginInfo> pluginInfos;
                         if (!csvLoader.TryLoadCSV(out pluginInfos)) {
                             logger.Error("Failed to load CSV, bailing.");
                             Environment.Exit(1);
                         }

                         Task.Run(async () => {
                             foreach (var pluginInfo in pluginInfos) {
                                 logger.Verbose($"Found Plugin: {pluginInfo.Name} {pluginInfo.Version} // GenerateBatchInstaller: {pluginInfo.GenerateBatchInstaller}");
                                 if (pluginInfo.GenerateBatchInstaller) {
                                     var batchGen = services.GetRequiredService<BatchInstallGenerator>();
                                     batchGen.PluginInfo = pluginInfo;
                                     await batchGen.Generate();
                                 }
                             }
                         }).Wait();
                     }
                 });
        }

        private static ServiceProvider ConfigureServices(ProgramOptions options, Logger logger) {
            var collection = new ServiceCollection()
                .AddSingleton(options)
                .AddSingleton(logger)
                .AddSingleton<CSVLoader>()
                .AddTransient<BatchInstallGenerator>()
                .AddTransient<PluginFileInspector>();

            return collection.BuildServiceProvider();
        }
    }
}
