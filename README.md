# dpbatchgen

Generates batch install files from accpp decal_plugins.csv data.

## Installing

- Requires [dotnet core 5.0 runtime](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/runtime-5.0.13-windows-x64-installer)
- Download latest dpbatchgen-version.zip from [Releases](https://gitlab.com/trevis/dpbatchgen/-/releases)
- Extract zip contents somewhere and run dpbatchgen.exe

## Tips

- If the `Direct URL` in the csv is a zip that contains multiple plugins/filters, they will all be installed/registered.
- The batch installers must be run as administrator in order to write to the registry (to add to decal).
- Currently supports Plugins and NetworkFilters.
- There is an [example decal_plugins.csv](https://gitlab.com/trevis/dpbatchgen/-/blob/master/decal_plugins.example.csv) included in this repository as an example format of what this app exects

## Usage

```
>dpbatchgen.exe --help
dpbatchgen 1.0.0

  -o, --out-path    (Default: ./decal plugins/current/{name}/{version}/{name}-{version}-install.bat) The output path for
                    each plugin batch installer.  You can use variables {name}, {version}, {clsid}, and {id}

  -c, --csv-path    (Default: ./decal_plugins.csv) Path to decal_plugins.csv, containing all the plugin info

  -d, --dry-run     (Default: false) Dry Run. Don't actually write any batch installer files.

  -v, --verbose     (Default: false) Verbose output

  --help            Display this help screen.

  --version         Display version information.
```
